import httpProxy from 'http-proxy';

const API_URL = process.env.NEXT_PUBLIC_API_URL_ACCOUNT_SERVICE ?? 'http://localhost:8000/v1';

const proxy = httpProxy.createProxyServer();

export async function GET(req: any, res: any): Promise<void> {
  return await new Promise<void>((resolve, reject) => {
    proxy.web(req, res, { target: API_URL, changeOrigin: true }, err => {
      if (err) {
        return reject(err);
      }
      resolve();
    });
  });
}
