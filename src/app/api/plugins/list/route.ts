interface PluginsData {
  id: string;
  name: string;
  uploaded: string;
  issuer: string;
}

export function GET(req: Request, res: Response): Response {
  return new Response(
    JSON.stringify([
      {
        id: '1',
        name: 'Tokens',
        uploaded: '12.12.2023',
        issuer: 'example',
      },
      {
        id: '2',
        name: 'Tokens',
        uploaded: '12.12.2023',
        issuer: 'example',
      },
      {
        id: '3',
        name: 'Tokens',
        uploaded: '12.12.2023',
        issuer: 'example',
      },
      {
        id: '4',
        name: 'Tokens',
        uploaded: '12.12.2023',
        issuer: 'example',
      },
    ] satisfies PluginsData[]),
    {
      headers: {
        'content-type': 'application/json;charset=UTF-8',
      },
    }
  );
}
