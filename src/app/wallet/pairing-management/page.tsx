'use client';

import { Button, Col, Container, Row } from 'react-bootstrap';
import css from './pairing-management.module.scss';
import Table, { type TableData } from '@/components/table/Table';
import { useContext, useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCirclePlus } from '@fortawesome/free-solid-svg-icons';
import PairingModal from '@/components/pairing-modal/PairingModal';
import type { DevicesData } from '@/service/types';
import { useApiData } from '@/service/apiService';
import { AppContext } from '@/store/AppContextProvider';

const PairingManagement = (): JSX.Element => {
  const { data, error } = useApiData<DevicesData[]>(
    'devicesList',
    `${
      process.env.NODE_ENV !== 'development' ? process.env.API_URL_ACCOUNT_SERVICE : '/api'
    }/tenants/1/api/devices/list`
  );
  const [tableData, setTableData] = useState<TableData>();
  const [showModal, setShowModal] = useState(false);
  const { setError } = useContext(AppContext);

  useEffect(() => {
    error && setError(error);
  }, [error]);

  useEffect(() => {
    if (data) {
      // make sure it's an array of objects (only used for the mock data)
      const devicesData = Array.isArray(data) ? data : [data];

      setTableData({
        head: Object.keys(devicesData[0]),
        body: devicesData.map(({ ...credentials }) => {
          return { ...credentials, id: credentials.connectionId };
        }),
      });
    }
  }, [data]);

  const handleCloseModal = (): void => {
    setShowModal(false);
  };

  const handleAddDevice = (): void => {
    setShowModal(true);
  };

  return (
    <>
      <Container fluid>
        <Row className="mb-4">
          <Col
            md="6"
            sm="12"
            className={`${css['flex-center']} justify-content-between gap-2 mb-2`}
          >
            <div className="d-flex gap-2 align-items-center">
              <h1 className="mb-0">Devices</h1>
              <Button
                variant="light"
                className={`rounded-circle ${css['btn-add']}`}
                onClick={handleAddDevice}
              >
                <FontAwesomeIcon
                  icon={faCirclePlus}
                  className={css.icon}
                />
              </Button>
            </div>
          </Col>
        </Row>

        <Table data={tableData}></Table>
      </Container>

      <PairingModal
        show={showModal}
        handleClose={handleCloseModal}
      />
    </>
  );
};

export default PairingManagement;
