'use client';

import css from './did.module.scss';
import { Col, Container, Row } from 'react-bootstrap';
import Table, { type TableData } from '@/components/table/Table';
import type { DidData } from '@/service/types';
import { useApiData } from '@/service/apiService';
import { useContext, useEffect, useState } from 'react';
import { AppContext } from '@/store/AppContextProvider';

const Did = (): JSX.Element => {
  const { data, error } = useApiData<DidData[]>(
    'didList',
    `${
      process.env.NODE_ENV !== 'development' ? process.env.API_URL_ACCOUNT_SERVICE : '/api'
    }/tenants/1/api/kms/did/list`
  );
  const [tableData, setTableData] = useState<TableData>();
  const { setError } = useContext(AppContext);

  useEffect(() => {
    error && setError(error);
  }, [error]);

  useEffect(() => {
    if (data) {
      // make sure it's an array of objects (only used for the mock data)
      const didData = Array.isArray(data) ? data : [data];

      setTableData({
        head: Object.keys(didData[0]),
        body: didData.map(({ ...did }) => did),
      });
    }
  }, [data]);

  return (
    <Container fluid>
      <Row className="mb-4">
        <Col
          md="6"
          sm="12"
          className={`${css['flex-center']} justify-content-between gap-2 mb-2`}
        >
          <div className="d-flex gap-2 align-items-center">
            <h1 className="mb-0">DIDs</h1>
          </div>
        </Col>
      </Row>

      <Table data={tableData}></Table>
    </Container>
  );
};

export default Did;
