import { Button, Col, Container, Row } from 'react-bootstrap';
import css from './wallet.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowDownLong, faCirclePlus, faList } from '@fortawesome/free-solid-svg-icons';
import Divider from '@/components/divider/Divider';
import CardDocument, { type CardDocumentProps } from '@/components/card-document/CardDocument';

const documents: CardDocumentProps[] = [
  {
    title: 'ID Card',
    surname: 'Doe',
    name: 'John',
    birthday: '01/01/1970',
    birthplace: 'London',
    expirationDate: '01/01/2025',
    image: '/eu_id_card.png',
    flag: '/eu_flag.jpg',
  },
  {
    title: 'Passport',
    surname: 'Doe',
    name: 'John',
    birthday: '01/01/1970',
    birthplace: 'London',
    expirationDate: '01/01/2025',
    image: '/german_id_card.png',
    flag: '/german_flag.png',
  },
  {
    title: 'Driving License',
    surname: 'Doe',
    name: 'John',
    birthday: '01/01/1970',
    birthplace: 'London',
    expirationDate: '01/01/2025',
    image: '/german_driving_license.png',
    flag: '/eu_flag.jpg',
  },
];

const Wallet = (): JSX.Element => {
  return (
    <Container
      fluid
      className="mb-5"
    >
      <Row>
        <Col
          md="6"
          sm="12"
          className={`${css['flex-center']} justify-content-between gap-2 mb-2`}
        >
          <div className="d-flex gap-2 align-items-center">
            <h1 className="mb-0">Your Documents</h1>
            <Button
              variant="light"
              className={`rounded-circle ${css['btn-add']}`}
            >
              <FontAwesomeIcon
                icon={faCirclePlus}
                className={css.icon}
              />
            </Button>
          </div>
        </Col>
        <Col
          md="6"
          sm="12"
          className={`${css['flex-center']} justify-content-end gap-2`}
        >
          <Button
            variant="light"
            className={`${css['flex-center']} gap-1`}
          >
            <FontAwesomeIcon
              icon={faArrowDownLong}
              width={20}
              height={20}
            />
            Date
          </Button>
          <Button
            className={`${css['flex-center']} gap-1`}
            variant="light"
          >
            <FontAwesomeIcon
              icon={faList}
              width={20}
              height={20}
            />
            View
          </Button>
        </Col>
      </Row>

      <Divider className="my-2" />

      <div className={css['cards-container']}>
        {documents.map((document, index) => (
          <CardDocument
            key={index}
            {...document}
          />
        ))}
      </div>
    </Container>
  );
};

export default Wallet;
