'use client';

import { useContext, useEffect, useState } from 'react';
import css from './selection.module.scss';
import { Col, Container, Row } from 'react-bootstrap';
import Table, { type TableData } from '@/components/table/Table';
import type { CredentialsData } from '@/service/types';
import { useApiData } from '@/service/apiService';
import { AppContext } from '@/store/AppContextProvider';

const Selection = (): JSX.Element => {
  const { data, error } = useApiData<CredentialsData[]>(
    'selectionList',
    `${
      process.env.NODE_ENV !== 'development' ? process.env.API_URL_ACCOUNT_SERVICE : '/api'
    }/tenants/1/api/credentials/list`
  );
  const [tableData, setTableData] = useState<TableData>();
  const { setError } = useContext(AppContext);

  useEffect(() => {
    error && setError(error);
  }, [error]);

  useEffect(() => {
    if (data) {
      // make sure it's an array of objects (only used for the mock data)
      const selectionData = Array.isArray(data) ? data : [data];

      setTableData({
        head: Object.keys(selectionData[0]),
        body: selectionData.map(({ ...did }) => did),
      });
    }
  }, [data]);

  return (
    <Container fluid>
      <Row className="mb-4">
        <Col
          md="6"
          sm="12"
          className={`${css['flex-center']} justify-content-between gap-2 mb-2`}
        >
          <div className="d-flex gap-2 align-items-center">
            <h1 className="mb-0">Credentials</h1>
          </div>
        </Col>
      </Row>

      <Table
        data={tableData}
        showId
      ></Table>
    </Container>
  );
};

export default Selection;
