import WalletHeader from '@/components/header/WalletHeader';
import WalletSideMenu from '@/components/side-menu/WalletSideMenu';
import '../../scss/globals.scss';
import css from './layout.module.scss';

import { WalletProvider } from '@/store/Provider';
import { type Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Wallet',
  icons: ['/images/favicon.ico'],
};

const WalletLayout = ({ children }: { children: React.ReactNode }): JSX.Element => {
  return (
    <html lang="en">
      <body className={css.wrapper}>
        <WalletProvider>
          <WalletSideMenu />
          <div className={css['content-wrapper']}>
            <div className={css.content}>
              <WalletHeader />
              {children}
            </div>
          </div>
        </WalletProvider>
      </body>
    </html>
  );
};

export default WalletLayout;
