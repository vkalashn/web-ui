'use client';

import { RemoteComponent } from '@/RemoteComponent';
import { type PluginDiscoveryResponse } from '@/components/side-menu/WalletSideMenu';
import { useQueryClient } from '@tanstack/react-query';
import { useEffect, useState } from 'react';

interface PluginProps {
  params: {
    plugin: string;
  };
}

const Plugin = ({ params }: PluginProps): JSX.Element => {
  const [PluginComponent, setPluginComponent] = useState<JSX.Element>();
  const pluginDiscoveryData = useQueryClient().getQueryData<PluginDiscoveryResponse>(['pluginDiscovery']);

  const fetchPluginData = (pluginDiscovery: PluginDiscoveryResponse, pluginRoute: string): void => {
    const foundPlugin = pluginDiscovery.plugins.find(plugin => plugin.route === pluginRoute);

    setPluginComponent(<RemoteComponent url={foundPlugin?.url ?? ''} />);
  };

  useEffect(() => {
    if (!pluginDiscoveryData) {
      setPluginComponent(<div>Plugin not found</div>);
    } else {
      fetchPluginData(pluginDiscoveryData, params.plugin);
    }
  }, [pluginDiscoveryData, params.plugin]);

  return <div className="min-vh-100">{PluginComponent}</div>;
};

export default Plugin;
