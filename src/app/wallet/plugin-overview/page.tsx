'use client';

import Table, { type TableData } from '@/components/table/Table';
import { useContext, useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import FormCheckInput from 'react-bootstrap/esm/FormCheckInput';
import css from './plugin-overview.module.scss';
import type { PluginsData } from '@/service/types';
import { useApiData } from '@/service/apiService';
import { AppContext } from '@/store/AppContextProvider';

const PluginOverview = (): JSX.Element => {
  const { data, error } = useApiData<PluginsData[]>('pluginsList', '/api/plugins/list');
  const [tableData, setTableData] = useState<TableData>();
  const { setError } = useContext(AppContext);

  useEffect(() => {
    error && setError(error);
  }, [error]);

  useEffect(() => {
    if (data) {
      // make sure it's an array of objects (only used for the mock data)
      const pluginsData = Array.isArray(data) ? data : [data];

      setTableData({
        head: Object.keys(pluginsData[0]),
        body: pluginsData.map(({ ...did }) => did),
      });
    }
  }, [data]);

  return (
    <Container fluid>
      <Row className="mb-4">
        <Col
          md="6"
          sm="12"
          className={`${css['flex-center']} justify-content-between gap-2 mb-2`}
        >
          <div className="d-flex gap-2 align-items-center">
            <h1 className="mb-0">Plugins</h1>
          </div>
        </Col>
      </Row>

      <Table
        data={tableData}
        showActions
      >
        <Table.Actions>
          <div className={`d-flex gap-2`}>
            <FormCheckInput
              id="showMenu"
              type="checkbox"
            />
            <label htmlFor="showMenu">Show on Menu</label>
          </div>
        </Table.Actions>
      </Table>
    </Container>
  );
};

export default PluginOverview;
