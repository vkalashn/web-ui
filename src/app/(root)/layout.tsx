import type { Metadata } from 'next';
import '../../scss/globals.scss';
import Header from '@/components/header/Header';
import Footer from '@/components/footer/Footer';
import { AppProvider } from '@/store/Provider';

export const metadata: Metadata = {
  title: 'PCM Web',
  description: 'Personal Credential Manager Web Interface',
  icons: ['/images/favicon.ico'],
};

const RootLayout = ({ children }: { children: React.ReactNode }): JSX.Element => {
  return (
    <html lang="en">
      <body>
        <AppProvider>
          <div className="min-vh-100 d-flex flex-column">
            <Header />
            <div>{children}</div>
            <Footer />
          </div>
        </AppProvider>
      </body>
    </html>
  );
};

export default RootLayout;
