export interface MenuItem {
  title: string;
  id: string;
  // subitems for dropdown
  items?: MenuItem[];
  // path to internal route or external website
  path: string;
}

export const menuItems: MenuItem[] = [
  {
    title: 'Federation',
    id: 'federation',
    path: '/wallet/credentials',
    items: [
      {
        title: 'Credentials',
        id: 'credentials',
        path: '/wallet/credentials',
      },
      {
        title: 'Tokens',
        id: 'tokens',
        path: '/wallet/tokens',
      },
      {
        title: 'DIDs',
        id: 'dids',
        path: '/wallet/dids',
      },
      {
        title: 'Keys',
        id: 'keys',
        path: '/wallet/keys',
      },
    ],
  },
];
