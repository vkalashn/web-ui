import { menuItems } from '@/utils/headerData';
import GxfsNavDropdown from './GxfsNavDropdown';
import css from './Header.module.scss';

const NavigationBar = (): JSX.Element => {
  return (
    <div className={`${css['flex-center']} gap-2 d-none d-md-flex ${css.navigation}`}>
      {menuItems.map(item => {
        return (
          <div key={item.id}>
            <GxfsNavDropdown menuItem={item}></GxfsNavDropdown>
          </div>
        );
      })}
    </div>
  );
};

export default NavigationBar;
