import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import css from './WalletHeader.module.scss';
import { Button, Nav, NavItem, Navbar, NavbarCollapse } from 'react-bootstrap';
import { faSearch } from '@fortawesome/free-solid-svg-icons/faSearch';
import { faBell } from '@fortawesome/free-solid-svg-icons/faBell';
import { faArrowsRotate } from '@fortawesome/free-solid-svg-icons/faArrowsRotate';
import AccountButton from './AccountButton';

const WalletHeader = (): JSX.Element => {
  return (
    <Navbar
      className={`mb-4 shadow ${css.navbar}`}
      sticky="top"
    >
      {/*  <!-- Actions --> */}
      <NavbarCollapse>
        <Nav className={css.nav}>
          {/*  <!-- Search --> */}
          <NavItem className={`${css['nav-action-item']}`}>
            <Button
              variant="outline-light"
              className="text-dark border-0"
            >
              <FontAwesomeIcon
                icon={faSearch}
                className={css.icon}
              />
              <span className="d-sm-inline d-none">Search</span>
            </Button>
          </NavItem>

          {/*  <!-- Arrow Rotate --> */}
          <NavItem className={css['nav-action-item']}>
            <Button
              variant="outline-light"
              className="text-dark border-0"
            >
              <FontAwesomeIcon
                icon={faArrowsRotate}
                className={css.icon}
              />
              <span className="d-sm-inline d-none">Refresh</span>
            </Button>
          </NavItem>

          {/*  <!-- Notifications --> */}
          <NavItem className={css['nav-action-item']}>
            <Button
              variant="outline-light"
              className="text-dark border-0"
            >
              <FontAwesomeIcon
                icon={faBell}
                className={css.icon}
              />
              <span className="d-sm-inline d-none">Alerts</span>
            </Button>
          </NavItem>

          {/* <!-- User Information --> */}
          <NavItem className={css['nav-action-item']}>
            <AccountButton />
          </NavItem>
        </Nav>
      </NavbarCollapse>
    </Navbar>
  );
};

export default WalletHeader;
