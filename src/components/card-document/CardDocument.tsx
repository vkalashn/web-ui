import React from 'react';
import { Col, Container, Image, Row } from 'react-bootstrap';
import css from './CardDocument.module.scss';

export interface CardDocumentProps {
  title: string;
  surname: string;
  name: string;
  birthday: string;
  birthplace: string;
  expirationDate: string;
  image: string;
  flag: string;
}

const CardDocument = ({
  title,
  surname,
  name,
  birthday,
  birthplace,
  expirationDate,
  image,
  flag,
}: CardDocumentProps): JSX.Element => {
  return (
    <div
      className={`${css['card-document']} shadow`}
      style={{ backgroundImage: `url(${image})` }}
    >
      <Container>
        <Row className="p-1 flex-nowrap">
          <Col>
            <Image
              src={flag}
              width={50}
              alt="flag"
            />
          </Col>
          <Col
            xxl={11}
            className="d-flex flex-column justify-content-center align-items-start"
          >
            <h2>Verified EU Digital Identity</h2>
            <span>{title}</span>
          </Col>
        </Row>

        <Row>
          <Col xxl={12}>
            <span>Surname</span>
          </Col>
          <Col xxl={12}>
            <strong>{surname}</strong>
          </Col>
        </Row>

        <Row>
          <Col xxl={12}>
            <span>Name</span>
          </Col>
          <Col xxl={12}>
            <strong>{name}</strong>
          </Col>
        </Row>

        <Row>
          <Col xxl={12}>
            <span>Birthday</span>
          </Col>
          <Col xxl={12}>
            <strong>{birthday}</strong>
          </Col>
        </Row>

        <Row className="flex-nowrap">
          <Col>
            <span>Expiration Date</span>
          </Col>

          <Col>
            <span>Birthplace</span>
          </Col>
        </Row>

        <Row className="flex-nowrap">
          <Col>
            <strong>{expirationDate}</strong>
          </Col>
          <Col>
            <strong>{birthplace}</strong>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default CardDocument;
