'use client';

import { Table as BTable, Button } from 'react-bootstrap';
import css from './Table.module.scss';
import { useState } from 'react';
import PluginModal from '../plugin-modal/PluginModal';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFolder } from '@fortawesome/free-solid-svg-icons/faFolder';

export interface TableBody {
  id: string;
  [key: string]: string;
}

export interface TableData {
  head: string[];
  body: TableBody[];
}

interface TableProps {
  data?: TableData;
  children?: React.ReactNode;
  showId?: boolean;
  showActions?: boolean;
}

const Table = ({ data, children, showId = false, showActions = false }: TableProps): JSX.Element => {
  const [showModal, setShowModal] = useState(false);
  const [rowData, setRowData] = useState<TableBody>();

  const handleDetailsClick = (data: TableBody): void => {
    setRowData(data);
    setShowModal(true);
  };

  const handleCloseModal = (): void => {
    setShowModal(false);
  };

  return (
    <>
      {data ? (
        <BTable
          className={`${css.table}`}
          responsive
          borderless
        >
          <thead>
            <tr>
              {data.head.map((head, i) => {
                if (head === 'id' && !showId) return null;
                return <th key={i}>{head}</th>;
              })}
              {showActions && <th></th>}
            </tr>
          </thead>
          <tbody>
            {data.body.map(body => (
              <tr key={body.id}>
                {Object.keys(body).map((key, i) => {
                  if (key === 'id' && !showId) return null;
                  return <td key={i}>{body[key]}</td>;
                })}

                {showActions && (
                  <td className={css.actions}>
                    <Button
                      variant="light"
                      onClick={() => handleDetailsClick(body)}
                    >
                      See Details
                    </Button>
                    {children}
                  </td>
                )}
              </tr>
            ))}
          </tbody>
        </BTable>
      ) : (
        <div className={`${css['flex-center']} flex-column gap-2 ${css['no-data']}`}>
          <FontAwesomeIcon
            icon={faFolder}
            className={css.icon}
          />
          <h2>No data found</h2>
        </div>
      )}

      {rowData && (
        <PluginModal
          show={showModal}
          handleClose={handleCloseModal}
          data={rowData}
        />
      )}
    </>
  );
};

Table.Actions = ({ children }: { children: React.ReactNode }) => {
  return children;
};

export default Table;
