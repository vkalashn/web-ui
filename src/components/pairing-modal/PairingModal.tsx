'use client';

import css from './PairingModal.module.scss';
import { Image, Modal, ModalBody, ModalHeader, ModalTitle, Spinner } from 'react-bootstrap';
import { useQuery } from '@tanstack/react-query';
import { genericFetch } from '@/service/apiService';
import { useContext, useEffect } from 'react';
import { AppContext } from '@/store/AppContextProvider';

interface DevicesData {
  qrCodeLink: string;
}

interface PairingModalProps {
  show: boolean;
  handleClose: () => void;
}

const getQrCode = async (): Promise<DevicesData> => {
  return await genericFetch<DevicesData>(
    `${
      process.env.NODE_ENV !== 'development' ? process.env.API_URL_ACCOUNT_SERVICE : '/api'
    }/tenants/1/api/devices/link`
  );
};

const PairingModal = ({ show, handleClose }: PairingModalProps): JSX.Element => {
  const { data, isLoading, error } = useQuery({ queryKey: ['devicePairing'], queryFn: getQrCode });
  const { setError } = useContext(AppContext);

  useEffect(() => {
    error && setError(error);
  }, [error]);

  return (
    <Modal
      show={show}
      onHide={handleClose}
      centered
    >
      <ModalHeader closeButton>
        <ModalTitle>Device QR-Code</ModalTitle>
      </ModalHeader>
      <ModalBody className={`${css['flex-center']} p-5`}>
        {isLoading ? (
          <Spinner
            animation="border"
            variant="primary"
          />
        ) : (
          <div className={css['qr-code']}>
            <Image
              src={data?.qrCodeLink}
              alt="QR-Code"
              width={300}
            />
          </div>
        )}
      </ModalBody>
    </Modal>
  );
};

export default PairingModal;
