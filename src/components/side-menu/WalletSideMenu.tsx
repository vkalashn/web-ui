'use client';

import useScreenSize from '@/hooks/useScreenSize';
import {
  faBars,
  faChevronLeft,
  faCircleQuestion,
  faGear,
  faHistory,
  faShield,
  faWallet,
  type IconDefinition,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { useEffect, useState } from 'react';
import { Button, Image, Nav, NavbarBrand, NavItem } from 'react-bootstrap';
import css from './WalletSideMenu.module.scss';
import { useQueryClient } from '@tanstack/react-query';

export interface Plugin {
  name: string;
  route: string;
  url: string;
}

export interface PluginDiscoveryResponse {
  plugins: Plugin[];
}

interface LinkChild {
  name: string;
  href: string;
}

interface NavLink {
  name: string;
  icon: IconDefinition;
  children: LinkChild[];
}

const navLinks: NavLink[] = [
  {
    name: 'Credentials',
    icon: faWallet,
    children: [
      {
        name: 'Overview',
        href: '/wallet/credentials',
      },
      {
        name: 'Issuance',
        href: '/wallet/issuers',
      },
      {
        name: 'Presentation',
        href: '/wallet/selection',
      },
      {
        name: 'Offering',
        href: '/wallet/verifiers',
      },
    ],
  },
  {
    name: 'Settings',
    icon: faGear,
    children: [
      {
        name: 'Plugin Overview',
        href: '/wallet/plugin-overview',
      },
      {
        name: 'Pairing Management',
        href: '/wallet/pairing-management',
      },
      {
        name: 'Identity Overview',
        href: '/wallet/did',
      },
    ],
  },
];

const WalletSideMenu = (): JSX.Element => {
  const [open, setOpen] = useState(false);
  const screenSize = useScreenSize();
  const pathname = usePathname();
  const [links, setLinks] = useState(navLinks);
  const pluginDiscoveryData = useQueryClient().getQueryData<PluginDiscoveryResponse>(['pluginDiscovery']);

  useEffect(() => {
    if (!pluginDiscoveryData) return;

    const filteredLinks = links.filter(link => link.name !== 'Plugins');

    const pluginParent: NavLink = {
      name: 'Plugins',
      icon: faWallet,
      children:
        pluginDiscoveryData?.plugins.map((plugin: Plugin) => ({
          name: plugin.name,
          href: `/wallet/plugins/${plugin.route}`,
        })) ?? [],
    };

    setLinks([...filteredLinks, pluginParent]);
  }, []);

  useEffect(() => {
    if (screenSize.width > 768 && open) {
      setOpen(false);
    }
  }, [screenSize.width, open]);

  useEffect(() => {
    if (open) {
      setOpen(false);
    }
  }, [pathname]);

  const toggle = (): void => {
    setOpen(!open);
  };

  const isActiveLink = (href: string): boolean => {
    return pathname === href;
  };

  return (
    <>
      {/*  <!-- Hamburger Button --> */}
      <Button
        className={`${css['navbar-toggle']} text-dark border-0`}
        variant="outline-light"
        onClick={toggle}
      >
        <FontAwesomeIcon
          icon={faBars}
          className={css.icon}
        />
      </Button>

      <div className={`${css['sidebar-wrapper']} ${open ? css['wallet-sidebar-open'] : ''}`}>
        {/*  <!-- Sidebar --> */}
        <Nav className={`${css['wallet-sidebar']} flex-nowrap`}>
          <div>
            {/*  <!-- Sidebar - Brand --> */}
            <NavbarBrand className="flex-center">
              <Link href="/">
                <Image
                  className={css['gaia-logo']}
                  src="/icons/GXFS_logo_White.png"
                  alt="Logo"
                />
              </Link>
            </NavbarBrand>

            <div className="p-1">
              {links.map((link: NavLink) => (
                <div
                  key={link.name}
                  className="p-2"
                >
                  <div className={`${css['nav-parent']} gap-2 mb-1 border-bottom pb-1`}>
                    <FontAwesomeIcon
                      icon={link.icon}
                      className={css.icon}
                    />
                    <strong>{link.name}</strong>
                  </div>
                  <div>
                    {link.children.map(child => (
                      <NavItem
                        className={`${css['sidebar-item']} ${css['sidebar-child']}`}
                        key={child.name}
                      >
                        <Link
                          className={`${css['sidebar-link']} ${isActiveLink(child.href) ? css.active : ''}`}
                          href={child.href}
                        >
                          <span>{child.name}</span>
                        </Link>
                      </NavItem>
                    ))}
                  </div>
                </div>
              ))}
            </div>
          </div>

          {/* Collapse sidebar button */}
          <Button
            className={`${css['sidebar-collapse-button']} ${open ? css['wallet-sidebar-open'] : ''}`}
            onClick={toggle}
          >
            <FontAwesomeIcon
              icon={faChevronLeft}
              className={css.icon}
            />
          </Button>

          {/* Navbar Footer */}
          <div className={`${css['navbar-footer']} mb-2 mt-5`}>
            <NavItem className={`${css['sidebar-item']}`}>
              <Link
                className={`${css['sidebar-link']}`}
                href="/wallet/history"
              >
                <FontAwesomeIcon
                  icon={faHistory}
                  className={css.icon}
                />
                <span>History</span>
              </Link>
            </NavItem>

            <NavItem className={`${css['sidebar-item']}`}>
              <Link
                className={`${css['sidebar-link']}`}
                href="/wallet/privacy"
              >
                <FontAwesomeIcon
                  icon={faShield}
                  className={css.icon}
                />
                <span>Privacy</span>
              </Link>
            </NavItem>

            <NavItem className={`${css['sidebar-item']}`}>
              <Link
                className={`${css['sidebar-link']}`}
                href="/wallet/help"
              >
                <FontAwesomeIcon
                  icon={faCircleQuestion}
                  className={css.icon}
                />
                <span>Help</span>
              </Link>
            </NavItem>
          </div>
        </Nav>
      </div>
    </>
  );
};

export default WalletSideMenu;
