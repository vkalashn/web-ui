import { Modal, ModalBody, ModalHeader, ModalTitle } from 'react-bootstrap';
import { type TableBody } from '../table/Table';
interface PluginModalProps {
  show: boolean;
  handleClose: () => void;
  data: TableBody;
}

const PluginModal = ({ show, handleClose, data }: PluginModalProps): JSX.Element => {
  return (
    <Modal
      show={show}
      onHide={handleClose}
      centered
    >
      <ModalHeader closeButton>
        <ModalTitle>Plugin Details</ModalTitle>
      </ModalHeader>
      <ModalBody>
        {data && (
          <div>
            <p>
              <span style={{ fontWeight: 'bold' }}>Plugin ID:</span> {data.id}
            </p>
            <p>
              <span style={{ fontWeight: 'bold' }}>Plugin Name:</span> {data.name}
            </p>
            <p>
              <span style={{ fontWeight: 'bold' }}>Plugin Upload Date:</span> {data.uploaded}
            </p>
            <p>
              <span style={{ fontWeight: 'bold' }}>Plugin Issuer:</span> {data.issuer}
            </p>
          </div>
        )}
      </ModalBody>
    </Modal>
  );
};

export default PluginModal;
