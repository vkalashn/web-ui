import { Button, Image, Stack } from 'react-bootstrap';
import css from './Footer.module.scss';
import CookieInfoSection from './CookieInfoSection';

interface CookieSettingsProps {
  close: () => void;
}

export interface CookieInfo {
  name: string;
  provider: string;
  purpose: string;
  cookieName: string;
  expiry: string;
}

const CookieSettings = ({ close }: CookieSettingsProps): JSX.Element => {
  const ESSENTIAL_COOKIE: CookieInfo = {
    name: 'Borlabs Cookie',
    provider: 'Owner of this website',
    purpose: 'Saves the visitors preferences selected in the Cookie Box of Borlabs Cookie.',
    cookieName: 'borlabs-cookie',
    expiry: '1 Year',
  };

  return (
    <Stack
      direction="vertical"
      className="mt-2 ml-2 mr-2"
      gap={2}
    >
      <div>
        <Stack direction="horizontal">
          <Image
            className={css['gaia-logo']}
            src="/GXFS_logo_alone_White.png"
            alt="GXFS Logo"
          />
          <div className={css['gxfs-font-white-xxl-bold']}>Privacy Preference</div>
        </Stack>
      </div>
      <div className={css['gxfs-font-white-xxxs-normal']}>
        Here you will find an overview of all cookies used. You can give your consent to whole categories or display
        further information and select certain cookies.
      </div>
      <div>
        <Button
          variant="dark"
          onClick={close}
        >
          Accept all
        </Button>
      </div>
      <div>
        <CookieInfoSection
          cookieType="Essential"
          descripton="Essential cookies enable basic functions and are necessary for the proper function of the website."
          info={ESSENTIAL_COOKIE}
        ></CookieInfoSection>
      </div>
    </Stack>
  );
};

export default CookieSettings;
