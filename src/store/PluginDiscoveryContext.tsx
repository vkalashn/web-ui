import { type PluginDiscoveryResponse } from '@/components/side-menu/WalletSideMenu';
import { genericFetch } from '@/service/apiService';
import { useQuery } from '@tanstack/react-query';
import { useContext, useEffect } from 'react';
import { Spinner } from 'react-bootstrap';
import { AppContext } from './AppContextProvider';

interface PluginDiscoveryContextProps {
  children: React.ReactNode;
}

const getPluginDiscovery = async (): Promise<PluginDiscoveryResponse> => {
  return await genericFetch<PluginDiscoveryResponse>('/api/plugins');
};

const PluginDiscoveryContext = ({ children }: PluginDiscoveryContextProps): JSX.Element => {
  const { isLoading, error } = useQuery({
    queryKey: ['pluginDiscovery'],
    queryFn: getPluginDiscovery,
  });
  const { setError } = useContext(AppContext);

  useEffect(() => {
    error && setError(error);
  }, [error]);

  if (isLoading) {
    return (
      <div
        className="position-absolute vw-100 vh-100 z-3 d-flex justify-content-center align-items-center"
        style={{ backgroundColor: '#f8f9fc' }}
      >
        <Spinner
          animation="border"
          variant="primary"
        />
      </div>
    );
  }

  return <>{children}</>;
};

export default PluginDiscoveryContext;
