'use client';

import LoginInterceptor from '@/components/login/LoginInterceptor';
import AppContextProvider from './AppContextProvider';
import PluginDiscoveryContext from './PluginDiscoveryContext';
import TanStackProvider from './TanStackProvider';
import RegisterLogin from '@/components/login/RegisterLogin';

export const WalletProvider = ({ children }: { children: React.ReactNode }): JSX.Element => {
  return (
    <AppContextProvider>
      <TanStackProvider>
        <LoginInterceptor>
          <RegisterLogin>
            <PluginDiscoveryContext>{children}</PluginDiscoveryContext>
          </RegisterLogin>
        </LoginInterceptor>
      </TanStackProvider>
    </AppContextProvider>
  );
};

export const AppProvider = ({ children }: { children: React.ReactNode }): JSX.Element => {
  return (
    <AppContextProvider>
      <TanStackProvider>
        <LoginInterceptor>{children}</LoginInterceptor>
      </TanStackProvider>
    </AppContextProvider>
  );
};
