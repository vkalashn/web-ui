import Container from '@/components/container/Container';
import css from './ReadMore.module.scss';
import { Image } from 'react-bootstrap';

interface ReadMoreProps {
  reversed?: boolean;
}

const ReadMore = ({ reversed }: ReadMoreProps): JSX.Element => {
  return (
    <Container className={`${css.wrapper} ${reversed ? css.reversed : ''} py-0 mb-5`}>
      <Image
        src="/news_bg.png"
        alt="Read more"
        className={css['image-content']}
      />
      <div className={css['text-content']}>
        <h3 className={css['text-primary']}>
          New Delta study: Progress and challenges in implementing Gaia-X Federation Services
        </h3>
        <p>
          The Delta study, designed as a continuation of the study “Strategies for building Gaia-X ecosystems using
          Gaia-X Federation Services”, provides insights into the current status and challenges of GXFS implementation
          in various funding projects.
        </p>
      </div>
    </Container>
  );
};

export default ReadMore;
