import css from './WelcomeText.module.scss';

const WelcomeText = (): JSX.Element => {
  return (
    <div className={`${css.wrapper}`}>
      <div className={css.text}>
        <h1>Welcome to the GXFS Demo Federation</h1>
        <p>
          The Demo Federation uses the developed code and provides a first-hand impression of Gaia-X Federation
          Services. The services are intended as a reference implementation.
        </p>
      </div>
    </div>
  );
};

export default WelcomeText;
